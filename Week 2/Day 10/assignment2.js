let dataPasien = [
  {
    nama: "john",
    status: "positif",
  },
  {
    nama: "mike",
    status: "suspect",
  },
  {
    nama: "mia",
    status: "positif",
  },
];

// cara ini akan menampilkan semua data; tidak efektif utk data ribuan
/* dataPasien.map(i=>{
    switch (i.status){
        case 'positif':
            console.log(i.nama);
            break;
        case 'suspect':
            console.log(i.nama);
            break
        default:
            console.log('all is well')
            }
        })
dataPasien */

//cara ini akan menampilkan hanya sesuai input

let input = "positif";

switch (input) {
  case "positif":
    dataPasien
      .filter((pasien) => pasien.status == "positif")
      .map((pasien) => {
        console.log(pasien.nama);
      });
    break;
  case "suspect":
    dataPasien
      .filter((pasien) => pasien.status == "suspect")
      .map((pasien) => {
        console.log(pasien.nama);
      });
    break;
  default:
    console.log("all is well!");
}
