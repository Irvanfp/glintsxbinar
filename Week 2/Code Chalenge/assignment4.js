function dataSort(dataRaw) {
  for (var i = 0; i < dataRaw.length; i++) {
    for (var j = 0; j < dataRaw.length - i - 1; j++) {
      if (dataRaw[j] > dataRaw[j + 1]) {
        var temp = dataRaw[j];
        dataRaw[j] = dataRaw[j + 1];
        dataRaw[j + 1] = temp;
      }
    }
  }
  return dataRaw;
}

module.exports = { dataSort };
