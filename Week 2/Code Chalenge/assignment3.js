const cc = require(`./assignment4`);
/*
 * DON'T CHANGE
 * */

const data = [];
const randomNumber = Math.floor(Math.random() * 100);
function createArray() {
  for (let i = 0; i < randomNumber; i++) {
    data.push(createArrayElement());
  }

  // Recursive
  if (data.length == 0) {
    createArray();
  }
}

function createArrayElement() {
  let random = Math.floor(Math.random() * 1000);

  return [null, random][Math.floor(Math.random() * 2)];
}

createArray();
/*
 * Code Here!
 * */

let dataClean = clean(data);
console.log(dataClean);

function clean(data) {
  // Code here
  let testdata = data.filter(dataNull => dataNull!==null);
  let result = cc.dataSort(testdata);
  return testdata;
}


/*
 * DON'T CHANGE
 * */

if (process.argv.slice(2)[0] == "test") {
  try {
    clean(data).forEach((i) => {
      if (i == null) {
        throw new Error("Array still contains null");
      }
    });
  } catch (err) {
    console.error(err.message);
  }
}
