// Import readline
const index = require("./utils"); // Import index to run rl on this file

/*function to calculate cone */
function calculateTriangularPrism(base, altitude, height) {
    console.log(`Base: ${base}, Altitude: ${altitude}, Height: ${height}`)
    let volume = 0.5 * base * altitude * height;
    return volume;
}

function inputBase() {
    index.rl.question(`Base (cm): `, base => {
        if (!isNaN(base) && (base > 0)) {
            inputAltitude(base);
        } else {
            console.log(`Base should be in number!`);
            inputBase();
        }
    })
}

function inputAltitude(base) {
    index.rl.question(`Altitude (cm): `, alt => {
        if (!isNaN(alt) && (alt > 0)) {
            inputHeight(base, alt);
        } else {
            console.log(`Altitude should be in number!`);
            inputAltitude(base);
        }

    })
}

function inputHeight(base, alt) {
    index.rl.question(`Height (cm): `, hgt => {
        if (!isNaN(hgt) && (hgt > 0)) {
            console.log(`Volume Triangular Prism is (cm): ${calculateTriangularPrism(base,alt,hgt)}`);
            index.rl.close()
        } else {
            console.log(`Height should be in number!`);
            inputHeight(base, alt);
        }
    })
}

module.exports = { inputBase, inputAltitude, inputHeight }