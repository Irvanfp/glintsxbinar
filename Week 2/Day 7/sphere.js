const { rl } = require("./utils"); // Import index to run rl on this file

function calculateSpereVolume(radius) {
    return 4 / 3 * Math.PI * Math.pow(radius, 3);
}

function throwError() {
    console.log('Length Must Be Number And Positive Number');
    calSphere();
}

function noError(getRad) {
    console.log(`Volume Sphere = ${calculateSpereVolume(getRad)}\n`);
    rl.close();
}

function calSphere() {
    rl.question(`Radius :`, (rad) => {

        let x = isNaN(rad) || rad <= 0 ? throwError() : noError(rad);
    });
}
module.exports = { calSphere };