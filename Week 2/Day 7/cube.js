const { rl } = require("./utils"); // Import index to run rl on this file

// Function to calculate cube volume
function calculateVolumeCube(length) {
  return length ** 3;
}

// Function to input the length
function input() {
  rl.question(`Length: `, (length) => {
    if (!isNaN(length) && (length > 0)) {
      console.log(`Cube's volume is ${calculateVolumeCube(length)}\n`);
      rl.close();
    } else {
      console.log(`Length must be number`);
      input();
    }
  });
}

module.exports = { input }; // Export the input, so the another file can run this code
