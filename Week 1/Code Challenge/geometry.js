//Geometry 1
function cubic(sisi){
    volume = sisi**3;
    return volume;
}

const kubus = cubic(7.3);
console.log("kubus ini memiliki volume: "+kubus+" Centimeter Kubik");

//Geometry 2
function beam(panjang, lebar, tinggi){
    volume = panjang*lebar*tinggi;
    return volume;
}

const balok = beam(11, 29, 37);
console.log("balok ini memiliki volume: "+balok+" Centimeter Kubik"); 