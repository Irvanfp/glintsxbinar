// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

//Geometry 2
function beam(panjang, lebar, tinggi) {
  volume = panjang * lebar * tinggi;
  return volume;
}

//asking panjang
function tanyaPanjang() {
  rl.question('Panjang: ',panjang=>{
    if (!isNaN(panjang)){
      tanyaLebar(panjang);
    } else {
      console.log(`panjang harus berupa angka!\n`);
      tanyaPanjang();
    }
  });
}

//asking lebar
function tanyaLebar(panjang) {
    rl.question('Lebar: ',lebar=>{
      if (!isNaN(lebar)){
        tanyaTinggi(panjang,lebar);
      } else {
        console.log(`lebar harus berupa angka!\n`);
        tanyaLebar();
      }
    });
  }


//asking tinggi
function tanyaTinggi(panjang,lebar) {
    rl.question('Tinggi: ',tinggi=>{
      if (!isNaN(tinggi)){
        console.log(`Volume: ${beam(panjang, lebar, tinggi)}`);
      } else {
        console.log(`tinggi harus berupa angka!\n`);
        tanyaTinggi();
      }
    });
  }  

  console.log(`Rectangle`);
  console.log(`=========`);
  tanyaPanjang();
  
