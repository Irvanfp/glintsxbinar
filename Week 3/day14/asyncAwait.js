const fs = require("fs");

function openFile(file, format) {
  return new Promise((resolve, reject) => {
    fs.readFile(file, format, (notRun, run) => {
      if (run) {
        return resolve(run);
      } else {
        return reject(notRun);
      }
    });
  });
}

let openFileAsync = async () => {
  try {
    file1 = await openFile("./files/file1.txt", "utf-8");
    file2 = await openFile("./files/file2.txt", "utf-8");
    file3 = await openFile("./files/file3.txt", "utf-8");
    file4 = await openFile("./files/file4.txt", "utf-8");
    file5 = await openFile("./files/file5.txt", "utf-8");
    file6 = await openFile("./files/file6.txt", "utf-8");
    file7 = await openFile("./files/file7.txt", "utf-8");
    file8 = await openFile("./files/file8.txt", "utf-8");
    file9 = await openFile("./files/file9.txt", "utf-8");
    file10 = await openFile("./files/file10s.txt", "utf-8");

    console.log(file1);
    console.log(file2);
    console.log(file3);
    console.log(file4);
    console.log(file5);
    console.log(file6);
    console.log(file7);
    console.log(file8);
    console.log(file9);
    console.log(file10);
  } catch (notRun) {
    console.log(notRun);
  }
};

openFileAsync()
