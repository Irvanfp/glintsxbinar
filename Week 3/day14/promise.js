const fs = require("fs");

function openFile(file, format) {
  return new Promise((resolve, reject) => {
    fs.readFile(file, format, (notRun, run) => {
      if (run) {
        return resolve(run);
      } else {
        return reject(notRun);
      }
    });
  });
}

openFile("./files/file1.txt", "utf-8")
  .then((run1) => {
    console.log(run1);
    return openFile("./files/file2.txt", "utf-8");
  })
  .then((run2) => {
    console.log(run2);
    return openFile("./files/file3.txt", "utf-8");
  })
  .then((run3) => {
    console.log(run3);
    return openFile("./files/file4.txt", "utf-8");
  })
  .then((run4) => {
    console.log(run4);
    return openFile("./files/file5.txt", "utf-8");
  })
  .then((run5) => {
    console.log(run5);
    return openFile("./files/file6.txt", "utf-8");
  })
  .then((run6) => {
    console.log(run6);
    return openFile("./files/file7.txt", "utf-8");
  })
  .then((run7) => {
    console.log(run7);
    return openFile("./files/file8.txt", "utf-8");
  })
  .then((run8) => {
    console.log(run8);
    return openFile("./files/file9.txt", "utf-8");
  })
  .then((run9) => {
    console.log(run9);
    return openFile("./files/file10.txt", "utf-8");
  })
  .then((run10) => {
    console.log(run10);
  })
  .catch((notRun) => {
    console.log(notRun);
  });
