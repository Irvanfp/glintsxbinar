const { index, Cube, Beam, Tube } = require("./threeDee");

// index.menu()
let cube = new Cube(10);
cube.volumeCube();

let beam = new Beam(10, 11, 12);
beam.volumeBeam();

let tube = new Tube(11, 19);
tube.volumeTube();
