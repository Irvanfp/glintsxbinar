const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
const Cube = require("./cube");
const Beam = require("./beam");
const Tube = require("./tube");

function menu() {
  console.log("which volume do you want to measure?");
  console.log("1. Cube");
  console.log("2. Beam");
  console.log("3. Tube");
  console.log("4. exit");
  rl.question("choose one: ", (option) => {
    if (option == 1) {
      let cube = new Cube(null);
      cube.introCube();
      rl.question(`please state length: `, (length) => {
        cube = new Cube(length);
        cube.volumeCube();
        rl.close();
      });
    } else if (option == 2) {
      let beam = new Beam(null);
      beam.introBeam();
      rl.question(`please state height: `, (height) => {
        rl.question(`please state width: `, (width) => {
          rl.question(`please state length: `, (length) => {
            beam = new Beam(height, width, length);
            beam.volumeBeam();
            rl.close();
          });
        });
      });
    } else if (option == 3) {
      let tube = new Tube(null);
      tube.introTube();
      rl.question(`please state height: `, (height) => {
        rl.question(`please state radius: `, (radius) => {
          tube = new Tube(height, radius);
          tube.volumeTube();
          rl.close();
        });
      });
    } else if (option == 4) {
      rl.close();
    } else {
      console.log("please choose the correct respond");
      menu();
    }
  });
}

rl.close()
module.exports = {menu, Cube, Tube, Beam};
