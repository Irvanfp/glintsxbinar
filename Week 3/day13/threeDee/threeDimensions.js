// const readline = require("readline");
// const rl = readline.createInterface({
//   input: process.stdin,
//   output: process.stdout,
// });

class Threedimension {
  constructor(name, height, width, length, radius) {
    this.name = name;
    this.length = length;
    this.width = width;
    this.height = height;
    this.radius = radius;
    // if (this.constructor == Threedimension) {
    //   throw new Error("should not declare object!");
    // }
  }
  introduction() {
    console.log(`This is 3D geometry`);
  }
  resultCube() {
    console.log(
      `\n The volume of the ${this.name} with length: ${this.length} cm, is: \n ===================`
    );
  }
  resultBeam() {
    console.log(
      `\n The volume of the ${this.name} with height: ${this.height} cm, length: ${this.length} cm, and width: ${this.width} cm, is: \n ===================`
    );
  }
  resultTube() {
    console.log(
      `\n The volume of the ${this.name} with height: ${this.length} cm and radius ${this.radius} cm, is: \n ===================`
    );
  }
}

module.exports = Threedimension;
