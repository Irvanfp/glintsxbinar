const Threedimension = require("./threeDimensions");

class Cube extends Threedimension {
  constructor(length) {
    super("cube", null, null, length, null);
  }
  introCube() {
    console.log(`you are choosing cube!`);
  }
  volumeCube() {
    super.resultCube();
    console.log(`Volume ${this.name}: ${this.length ** 3} cm`);
    
  }
}

module.exports = Cube;
