const Threedimension = require("./threeDimensions");

class Tube extends Threedimension {
  constructor(height, radius) {
    super("tube", height, null, null, radius);
  }
  introTube() {
    console.log(`you are choosing tube!`);
  }
  volumeTube() {
    super.resultTube();
    console.log(
      `Volume ${this.name}: ${Math.floor(
        Math.PI * this.radius ** 2 * this.height
      )} cm`
    );
  }
}



module.exports = Tube;
