const Threedimension = require("./threeDimensions");

class Beam extends Threedimension {
  constructor(height, width, length) {
    super("beam", height, width, length, null);
  }
  introBeam() {
    console.log(`you are choosing beam!`);
  }
  volumeBeam() {
    super.resultBeam();
    console.log(
      `Volume ${this.name}: ${this.length * this.height * this.width} cm`
    );
  }
}

module.exports = Beam;
