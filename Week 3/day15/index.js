const routes = require("./route/routeAssessment");
const express = require("express");
const app = express();

app.use("/", routes);

app.listen(5000, () => {
  console.log("running on port 5000");
});
