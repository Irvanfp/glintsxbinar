const express = require("express");
const ControllerAssessment = require("../control/controlAssessment");
const routes = express.Router();

routes.get("/:irvan", ControllerAssessment.get);

routes.post("/:irvan", ControllerAssessment.post);

routes.put("/:irvan", ControllerAssessment.put);

routes.delete("/:irvan", ControllerAssessment.delete);

module.exports = routes;
